var config = {};

config.select = 'Select';
config.ongoing = {
    'variety' : 'Variety',
    'trial' : 'Trial',
    'set_forget' : 'Set it & Forget it'
};
config.prefs = {
    'Joy of Food' : 'joy_of_food',
    'Food as Fuel': 'food_as_fuel',
    'Feeding Families': 'feeding_families',
    'Solution Seekers': 'solution_seekers'
};

config.res = {
    'nutri' : 'No problem. Here is the nutrition facts for %s',
    'ingredient' : 'Sure. Here is the ingredient list for %s',
    'replace_yes' : 'Great. We will keep the '
};
module.exports = config;