var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var mongo_url = "mongodb://devgalahadchat:JLzCfZlJmtJug3xbzsGswwSIcixKok2G4Cw99Dg6qG7h07Vkcd9PxITUg7wCX9JsJkLKFAl7VwD20MBrBB7TOA==@devgalahadchat.documents.azure.com:10255/meal_rec_db_id?ssl=true&replicaSet=globaldb";
var async = require('async');

function find_recipes(db, params,limit, callback) {
    //console.log(params);
    var cursor = db.collection('meal_rec').find(params).limit(limit).toArray();
    cursor.then((docs) => {
        //console.log(docs);
        callback(null, docs, db);
    });
}

function connect(callback) {
    MongoClient.connect(mongo_url, function (err, db) {
        assert.equal(null, err);
        callback(null, db);
    });
}

function insert_user(db, user, callback) { 
    db.collection('meal_rec_users').update(
        {
            user_id : user.user_id
        },
        user,
         // adds new user if no match 
        {
            upsert: true
        },
    (err, result) => { 
        assert.equal(null, err);
        console.log('inserted new user');
        callback(err, result);
    });
}

function insert_recipe(db, recipe, callback) { 
    db.collection('meal_rec').update(
        {
            id : recipe.id
        },
        recipe,
         // adds new user if no match 
        {
            upsert: true
        },
    (err, result) => { 
        assert.equal(null, err);
        console.log('inserted new recipe');
        callback(err, result);
    });
}


module.exports = {
    connect: connect,
    find_recipes: find_recipes,
    insert_user: insert_user,
    insert_recipe : insert_recipe
}

/*
var insertDocument = function (db, callback) {
    db.collection('meal_rec_users').insertOne({
        "user_id": "hadfs78992hkj",
        "protein0": "chicken",
        "protein1": "",
        "veg0": "lettuce",
        "veg1": "spinach",
        "carb": "bread",
        "time": "minimum",
        "prefs":"medium",
        "days": 6,
        "size" : 5,
        "diet_res": "vegan",
        "cuisine": "italian"
    }, function (err, result) {
        assert.equal(err, null);
        console.log("Inserted a document into the meal_rec collection.");
        callback();
    });
};
MongoClient.connect(mongo_url, function (err, db) {
    assert.equal(null, err);
    insertDocument(db, function () {
        db.close();
    });
});
*/
